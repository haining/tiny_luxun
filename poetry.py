"""
This module is used to scrape poetry written by Lu Xun in ziyexing.com.

```bash
scrapy runspider poetry.py -o raw.jsonl
```

"""

__author__ = "hw56@indiana.edu"
__version__ = "0.1.0"
__license__ = "0BSD"


import scrapy


class URLSpider(scrapy.Spider):
    name = 'poetry_spider'
    start_urls = ['http://www.ziyexing.com/luxun/luxun_shici/luxun_shici_1.htm',
                  'http://www.ziyexing.com/luxun/luxun_shici/luxun_shici_2.htm']

    def parse(self, response):

        yield {'content': response.css('div , p').get(),
               'url': response.url}
