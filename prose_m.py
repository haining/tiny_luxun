"""
This module is used to scrape prose written by Lu Xun in marxists.org.

```bash
scrapy runspider prose_m.py -o m_raw.jsonl
```

"""

__author__ = "hw56@indiana.edu"
__version__ = "0.1.0"
__license__ = "0BSD"


import scrapy


class URLSpider(scrapy.Spider):
    name = 'prose_spider_marxists'
    start_urls = ['https://www.marxists.org/chinese/reference-books/luxun/29/000.htm']

    css_selectors = {
        'https://www.marxists.org/chinese/reference-books/luxun/29/000.htm': 'br+ a::attr(href)',
    }


    def parse(self, response):

        css_selector = self.css_selectors[response.url]
        for href in response.css(css_selector):
            url = response.urljoin(href.extract())
            yield scrapy.Request(url, callback=self.parse_page)

    def parse_page(self, response):

        yield {'content': response.css('br+ p').get(),
               # this is messy
               'title': response.css('.title1::text').get(),
               'url': response.url}

