"""
This module is used to scrape prose written by Lu Xun in ziyexing.com.

```bash
scrapy runspider prose_zyx.py -o zyx_raw.jsonl
```

"""

__author__ = "hw56@indiana.edu"
__version__ = "0.1.0"
__license__ = "0BSD"


import scrapy


class URLSpider(scrapy.Spider):
    name = 'prose_spider_ziyexing'
    start_urls = [
                  'http://www.ziyexing.com/luxun/luxun_index.htm',
                  'http://www.ziyexing.com/luxun/zgxssl/zgxssl_index.htm',
                  'http://www.ziyexing.com/luxun/zgxs_yanjiang/zgxs_yanjiang_index.htm']

    css_selectors = {
        'http://www.ziyexing.com/luxun/luxun_index.htm': '#table02 a::attr(href)',
        'http://www.ziyexing.com/luxun/zgxssl/zgxssl_index.htm': '#table3 tr:nth-child(1) tr a::attr(href)',
        'http://www.ziyexing.com/luxun/zgxs_yanjiang/zgxs_yanjiang_index.htm':
            '#table3 tr:nth-child(1) tr a::attr(href)'
    }

    def parse(self, response):

        css_selector = self.css_selectors[response.url]
        for href in response.css(css_selector):
            url = response.urljoin(href.extract())
            yield scrapy.Request(url, callback=self.parse_page)

    def parse_page(self, response):

        yield {'content': response.css('div , p').get(),
               # this is messy
               'category_book_title': response.css('#table3 font::text').getall()[-1],
               'url': response.url}

